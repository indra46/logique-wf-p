# Logique performance list

## improve TTFB (TIme to First Bite)

respontime pertama kali hit ke logique **3.5s ++**. untuk report lengkap bisa dilihat dengan detail pada https://gtmetrix.com/reports/www.logique.co.id/vDfXB4Vk/

solusi:

- implement server side caching
- upgrade server hardware/ vertical scaling
- jika terasa sulit, dan tidak memungkinkan. assets dapat di pindahkan ke hosting/ cdn yang gratis atau berbayar. namun hal ini hanya mengurangi load time untuk assets, sedangkan TTFB masih tidak akan berubah

Detail https://gtmetrix.com/reduce-initial-server-response-time.html

## LCP `video#bgvid` improvement

Video asstes (background video) tetap di fetch di mobile version. padahal video tidak di load. terdapat beberapa solusi.

- Rubah design. tidak menggunakan video background

- Ubah src pada vide menjadi data-src. Detail source code dapat selengkapnya dilihat di https://stackoverflow.com/questions/43984623/stop-video-from-loading-in-background-on-mobile

## Preload fonts.

Terdapat beberapa fonts yang belum di preload yaitu logique.co.id/fonts/Novecentosanswide-Medium.otf.

preload font sebagai mana kita mempreload google font. namun karena font di simpan di server logique yang disana juga memiliki masalah dalam TTFB. baiknya font di ganti ke google font yg sudah ada. (merubah font-familt dengan font yg ada atau menggunakan resource google font)
